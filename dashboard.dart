import 'package:flutter/material.dart';
import 'package:four_developer/Screens/profile.dart';

void main(){
  runApp(const MyApp());

}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'DBSS',
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
        ),
        home: Scaffold(
          appBar: AppBar(title: const Text("DBSS Home")),
          body: DashboardA(),
        )
    );
  }
}
class DashboardA extends StatefulWidget{
  @override
  _DashboardAState createState() => _DashboardAState();
}
class _DashboardAState extends State<DashboardA>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {Navigator.push(context, MaterialPageRoute(
            builder: (context)=>ProfileA()));}, hoverColor: Colors.deepOrange,),
      body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(Icons.menu, color: Colors.black,size:35.0),
                      Image.asset("assets/user.png", width: 35.0)
                          // <a href="https://www.flaticon.com/free-icons/user" title="user icons">User icons created by Freepik - Flaticon</a>
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(16.0),
                  child:Text(
                    "Dashboard",
                    style: TextStyle(
                      color: Colors.deepOrangeAccent,
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Wrap(
                      spacing: 15.0,
                      runSpacing: 15.0,
                      children: [
                        SizedBox(
                          width: 160.0,
                            height:160.0,
                          child: Card(
                            color: Colors.grey,
                            elevation:2.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0)
                            ),
                            child:Center(
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Column(
                                  children: [
                                    Image.asset("assets/cctv.png", width:64.0),
                                    // <a href="https://www.flaticon.com/free-icons/cctv" title="cctv icons">Cctv icons created by Creative Stall Premium - Flaticon</a>
                                    SizedBox(height:10.0),
                                    const Text("View Footage", style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0
                                    )),

                                    SizedBox(height:5.0),
                                    const Text(" 8 Items", style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w100
                                    ))

                                  ],
                                ),
                              ),

                            )
                          ),
                        ),
                        SizedBox(
                          width: 160.0,
                          height:160.0,
                          child: Card(
                              color: Colors.grey,
                              elevation:2.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)
                              ),
                              child:Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    children: [
                                      Image.asset("assets/house.png", width:64.0),
                                      // <a href="https://www.flaticon.com/free-icons/house" title="house icons">House icons created by Freepik - Flaticon</a>
                                      SizedBox(height:10.0),
                                      const Text("Add Home", style: TextStyle(
                                          color: Colors.deepOrange,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0
                                      )),

                                      SizedBox(height:5.0),
                                      const Text(" 1 Item", style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w100
                                      ))

                                    ],
                                  ),
                                ),

                              )
                          ),
                        ),
                        SizedBox(
                          width: 160.0,
                          height:160.0,
                          child: Card(
                              color: Colors.grey,
                              elevation:2.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)
                              ),
                              child:Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    children: [
                                      Image.asset("assets/shield.png", width:64.0),
                                    // <a href="https://www.flaticon.com/free-icons/shield" title="shield icons">Shield icons created by Freepik - Flaticon</a>
                                    SizedBox(height:10.0),
                                      const Text("Armed Response", style: TextStyle(
                                          color: Colors.deepOrange,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.0
                                      )),


                                      SizedBox(height:5.0),
                                      const Text(" 1 Item", style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w100
                                      ))

                                    ],
                                  ),
                                ),

                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
  }
}


