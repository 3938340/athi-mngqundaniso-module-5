import 'package:flutter/material.dart';
import 'package:four_developer/Screens/register.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/src/widgets/gesture_detector.dart';
// import 'firebase_options.dart';
import 'dashboard.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}
class MyApp extends StatelessWidget{
  const MyApp({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'DBSS',
      home: LoginA(),
    );
  }
}
class LoginA extends StatefulWidget{
  @override
  _LoginAState createState() => _LoginAState();

}

class _LoginAState extends State<LoginA>{
  // controllers
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  get LogIn => null;

  // get LogIn => null;

  Future logIn() async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
    );
    FirebaseFirestore.instance.collection("SignInDetails")
        .add({"email":"", "password": ""});
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child:ListView(
          padding: const EdgeInsets.symmetric(horizontal: 14.0),
          children: <Widget>[
            Column(
              children: <Widget>[
                const SizedBox(height: 150,),
                Image.asset('assets/sicon.png'),
                const SizedBox(height: 25,),
                const Text('DBSS Login',style: TextStyle(fontSize: 15, color: Colors.orangeAccent),)
            ],
           ),
            const SizedBox(height:50.0,),
            TextField(
              controller: _emailController,
              decoration: InputDecoration(
                labelText: "Username/Email",
                labelStyle: TextStyle(fontSize:15),
                filled: true,
              ),
            ),
            const SizedBox(height:15.0,),
            TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    labelText: "Password",
                    labelStyle: TextStyle(fontSize:15),
                    filled: true,
                ),
            ),
        GestureDetector(
            onTap: LogIn,
        ),
            const SizedBox(height:15),
            Column(
              children: <Widget>[
                MaterialButton(
                  height: 50,
                  disabledColor: Colors.grey,
                  disabledElevation: 4.0,
                  onPressed: () { Navigator.push(context, MaterialPageRoute(
                      builder: (context)=>DashboardA()
                  )); },
                  
                  child: ElevatedButton(
                    onPressed: null,
                    child: Text('Login',style:TextStyle(fontSize:15,color: Colors.black)),
                  ),
                ),
                const SizedBox(height:15,),
                Column(
                    children: <Widget>[
                      MaterialButton(
                        height: 20,
                        disabledColor: Colors.grey,
                        disabledElevation: 4.0,
                        onPressed: () { Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>RegisterA()
                        ));  },
                        child:const ElevatedButton(
                          onPressed: null,
                          child: Text('Register',style:TextStyle(fontSize:15,color: Colors.black)),
                        ),
                      ),
                      const SizedBox(height:15,),
                      const Text('Forgot Password'),
              ]
            )
          ]
        )
      ]
    )
    )
    );
  }
}