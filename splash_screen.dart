import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:four_developer/Screens/login.dart';

void main(){
  runApp(MyApp());
}
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: AnimatedSplashScreen(
        splash: Image.asset(
          'assets/sicon.png',
        ),
        nextScreen: LoginA(),
        splashTransition: SplashTransition.decoratedBoxTransition,
        backgroundColor: Colors.orangeAccent,
      )
    );
  }
}